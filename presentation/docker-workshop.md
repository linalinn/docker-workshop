---
marp: true

theme: uncover
paginate: false
<!-- No bg image for now because code blocks are kind bad loooking  backgroundImage: url('https://nexus.lina.cloud/repository/public-hosted/svg/backgroud.svg' -->
---
# Docker Workshop
---

# Agenda

- What is Docker for?
- Container vs VM
- Common Elements of Docker
- Running and Building Containers
- Docker compose

---

# What is Docker for?

- Simplifying deployment process
- Providing Isolated environment


---
# Container ⚡ VM

![width:30cm](https://upload.wikimedia.org/wikipedia/commons/0/0a/Docker-containerized-and-vm-transparent-bg.png)

<!-- _footer: 'Image: https://commons.wikimedia.org/wiki/File:Docker-containerized-and-vm-transparent-bg.png' -->
---

## Common Elements of Docker
- Image ⚡ Container
- Container Runtime
- Registry

---
# Image ⚡ Container

- Image
  - Contains all the application data and its dependencies.
  - Consists of layers each containing the changes of each build step.
  - Is immutable but can be shared and built upon.
- Container
  - Is the "running" Image
  - Contains the running application process

---
# Container Runtime

- Runs the Container (lifecycle management) 
- Manages: Storage & Networking
- Loading Images from Registries
- Building Images

---
# Registry
- Image Storage

---

# Running and Building Containers

---
# Container Lifecycle

- Create
- Run
- Pause
- Stop

---
# docker run

`docker run --name mynginx -p 8080:80 -d nginx`

---
# docker run

<style scoped>
td { font-size:25px; }
</style>

| Argument | Description|
|:---------|:----------|
| --name <string> | Assign a name to the container|
| -p [<ip>:]<port>:<port> |Publish a container's port(s) to the host|
| -d | Run container in background and print container ID |

---
# docker ps

`docker ps`

---
# docker stop

`docker stop mynginx`

---
# docker logs

`docker logs mynginx`

---
# docker start

`docker start mynginx`

---
# docker exec

`docker exec -it mynginx bash`

---
# docker exec

<style scoped>
td { font-size: 30px; }
</style>

| Argument | Description|
|:---------|:----------|
| -i | Keep STDIN open even if not attached |
| -t | Allocate a pseudo-TTY |

---
# docker inspect

`docker inspect mynginx`

---
# Storage bind mounts

<style scoped>
    code { font-size: 22px;}
</style>

`docker run --name webserver -v ./mysite:/usr/share/nginx/html:ro -p 8080:80 -d nginx`

---

# Storage Volumes
<style scoped>
    code { font-size: 23px;}
</style>
`docker volume create my-vol`

`docker run --name webserver -v my-vol:/usr/share/nginx/html -p 8080:80 -d nginx`

---
# docker volume

`docker volume ls`
</br>
`docker volume rm my-vol`

---
# docker rm

`docker rm mynginx`

---
# docker image

`docker image`
</br>
`docker image ls`
</br>
`docker images`

---
# Building Containers
---
# docker build

`docker build . -t java-server:simple`


<!-- footer: 'Sources: https://gitlab.com/linalinn/docker-workshop' -->
---
<style scoped>
td { font-size: 20px; }
th {font-size: 24px; }
</style>
# Dockerfile
| Keyword | Syntax | Exsample |
|:--|:---|:--|
| FROM |FROM <image>[:<tag>] [as <name>] | FROM ubuntu:22.04 |
| RUN | RUN <command> | RUN apt-get install openjdk-17-jre |
| ENV | ENV <key>=<value> | ENV MY_VARIABLE=foo |
| COPY | COPY <src> <dest> | COPY myapp.jar /app |
| EXPOSE | EXPOSE <port> [<port>/<protocool>] | EXPOSE 8080 |
| CMD | CMD [“executable/param”, “param”] | CMD [“java”, “-jar”, “/app/myapp.jar”] |
| ENTRYPOINT | ENTRYPOINT [“executable”, “param”] | ENTRYPOINT [“/bin/bash”] |
| WORKDIR | WORKDIR path/to/workdir | WORKDIR “/work” |
| USER | USER <UID>[:<GID>] | USER myapp |

---
# docker run
<style scoped>
    code { font-size: 30px;}
</style>
`docker run -p 8080:8080 -e myvar=foo --rm java-server:simple`

---
# docker run

<style scoped>
td { font-size: 30px; }
</style>

| Argument | Description|
|:---------|:----------|
| -e <key>=<value> | Environment variables |
| --rm | Remove intermediate containers when it exits |

---
# Build Containers

`docker build . -t java-server:builder`

---
# docker tag
```
docker tag java-server:builder <username>/java-server:latest
```
---
# docker pull
`docker pull httpd:2.4`

---
# docker pull
`docker push <username>/java-server:latest`

---

# docker compose

---
# docker-compose.yml
```yaml
version: '3'
services:
  nginx:
    image: nginx
    ports:
      - "8080:8080"
    volumes:
      - ./mysite:/usr/share/nginx/html
    environment:
      - NGINX_PORT=8080
```

---
## docker compose with networks

```yaml
version: '3'
services:
  nginx:
    image: nginx
    networks:
      - mynetwork

networks:
  mynetwork:
```

---
### docker compose with depends_on

```yaml
version: '3'
services:
  myapp:
    image: myapp
    depends_on:
      - mydb
  mydb:
    image: postgres
    volumens:
      - ./pgdata:/var/lib/postgresql/data
```

---
# docker compose up

`docker compose up`

---
# docker compose start/stop

`docker compose start`
</br>
`docker compose stop`

---
# docker compose down

`docker compose down`