# Docker Workshop

---

## Requirements
 - [Docker](https://docs.docker.com/engine/install/)
 - [Docker Hub Account](https://hub.docker.com/signup)

## Documentations

### Images
- [Nginx](https://hub.docker.com/_/nginx)
- [Debian](https://hub.docker.com/_/debian)
- [Eclipse Temurin](https://hub.docker.com/_/eclipse-temurin)
- [Postgres](https://hub.docker.com/_/postgres)

### Docker
 - [Docker cli](https://docs.docker.com/engine/reference/run/)
   - [run](https://docs.docker.com/engine/reference/run/)
   - [ps](https://docs.docker.com/engine/reference/commandline/ps/)
   - [logs](https://docs.docker.com/engine/reference/commandline/logs/)
   - [start](https://docs.docker.com/engine/reference/commandline/start/)
   - [stop](https://docs.docker.com/engine/reference/commandline/stop/)
   - [exec](https://docs.docker.com/engine/reference/commandline/exec/)
   - [inspect](https://docs.docker.com/engine/reference/commandline/inspect/)
   - [volume](https://docs.docker.com/engine/reference/commandline/volume_create/)
   - [image](https://docs.docker.com/engine/reference/commandline/image/)
   - [build](https://docs.docker.com/engine/reference/commandline/build/)
   - [tag](https://docs.docker.com/engine/reference/commandline/tag/)
   - [push](https://docs.docker.com/engine/reference/commandline/push/)
   - [pull](https://docs.docker.com/engine/reference/commandline/pull/)
 - [Dockerfile](https://docs.docker.com/engine/reference/builder/)
 - [docker-compose.yml (v3)](https://docs.docker.com/compose/compose-file/compose-file-v3/)
   - [environments](https://docs.docker.com/compose/compose-file/compose-file-v3/#environment)
   - [volumes](https://docs.docker.com/compose/compose-file/compose-file-v3/#volumes)
   - [networks](https://docs.docker.com/compose/compose-file/compose-file-v3/#networks)
   - [depends_on](https://docs.docker.com/compose/compose-file/compose-file-v3/#depends_on)

