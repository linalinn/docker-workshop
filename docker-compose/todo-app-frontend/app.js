// define the API endpoint
const apiEndpoint = "/api/todos";

// add event listener to form submit event
document.getElementById("add-todo-form").addEventListener("submit", function(event) {
	event.preventDefault();
	// get form data
	const formData = {
		title: document.getElementById("title").value,
		description: document.getElementById("description").value
	};
	// send POST request to API with form data
	fetch(apiEndpoint, {
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify(formData)
	})
	.then(response => response.json())
	.then(todo => {
		// add new Todo to table
		const tableRow = document.createElement("tr");
		const titleCell = document.createElement("td");
		titleCell.textContent = todo.title;
		tableRow.appendChild(titleCell);
		const descriptionCell = document.createElement("td");
		descriptionCell.textContent = todo.description;
		tableRow.appendChild(descriptionCell);
		const completedCell = document.createElement("td");
		completedCell.textContent = todo.completed ? "Yes" : "No";
		tableRow.appendChild(completedCell);
		const deleteButton = document.createElement("button");
		deleteButton.textContent = "Delete";
		deleteButton.classList.add("btn", "btn-danger", "btn-sm");
		deleteButton.addEventListener("click", function() {
			// send DELETE request to API to delete Todo
			fetch(apiEndpoint + "/" + todo.id, {
				method: "DELETE"
			})
			.then(() => {
				// remove Todo from table
				tableRow.remove();
			})
			.catch(error => console.error(error));
		});
		const actionsCell = document.createElement("td");
		actionsCell.appendChild(deleteButton);
		tableRow.appendChild(actionsCell);
		document.getElementById("todo-table-body").appendChild(tableRow);
		// reset form
		document.getElementById("add-todo-form").reset();
	})
	.catch(error => console.error(error));
});

// get existing Todos from API
fetch(apiEndpoint)
.then(response => response.json())
.then(todos => {
	// add existing Todos to table
	todos.forEach(todo => {
		const tableRow = document.createElement("tr");
		const titleCell = document.createElement("td");
		titleCell.textContent = todo.title;
		tableRow.appendChild(titleCell);
		const descriptionCell = document.createElement("td");
		descriptionCell.textContent = todo.description;
		tableRow.appendChild(descriptionCell);
		const completedCell = document.createElement("td");
		completedCell.textContent = todo.completed ? "Yes" : "No";
		tableRow.appendChild(completedCell);
		const deleteButton = document.createElement("button");
		deleteButton.textContent = "Delete";
		deleteButton.classList.add("btn", "btn-danger", "btn-sm");
		deleteButton.addEventListener("click", function() {
			// send DELETE request to API to delete Todo
			fetch(apiEndpoint + "/" + todo.id, {
				method: "DELETE"
			})
			.then(() => {
				// remove Todo from table
				tableRow.remove();
			})
			.catch(error => console.error(error));
		});
		const actionsCell = document.createElement("td");
		actionsCell.appendChild(deleteButton);
		tableRow.appendChild(actionsCell);
		document.getElementById("todo-table-body").appendChild(tableRow);
	});
})
.catch(error => console.error(error));
