package cloud.lina.workshop.docker.todoapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class TodoController {

    @Autowired
    private TodoService todoService;

    @GetMapping("/api/todos")
    public List<Todo> findAll() {
        return todoService.findAll();
    }

    @GetMapping("/api/todos/{id}")
    public ResponseEntity<Todo> findById(@PathVariable Long id) {
        Optional<Todo> todo = todoService.findById(id);
        return todo.map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/api/todos")
    public Todo create(@RequestBody Todo todo) {
        return todoService.save(todo);
    }

    @PutMapping("/api/todos/{id}")
    public ResponseEntity<Todo> update(@PathVariable Long id, @RequestBody Todo updatedTodo) {
        Optional<Todo> todo = todoService.findById(id);
        if (todo.isPresent()) {
            updatedTodo.setId(id);
            todoService.save(updatedTodo);
            return ResponseEntity.ok(updatedTodo);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/api/todos/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        Optional<Todo> todo = todoService.findById(id);
        if (todo.isPresent()) {
            todoService.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}

